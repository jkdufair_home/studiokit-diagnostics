﻿using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights.TraceListener;
using StudioKit.Configuration;
using StudioKit.Encryption;
using System;
using System.Diagnostics;

namespace StudioKit.Diagnostics
{
	/// <summary>
	/// Provides a set of methods to trace messages to the debug console and Application Insights.
	/// </summary>
	public class Logger : ILogger
	{
		protected TraceSource TraceSource { get; set; }

		public Logger(string name) : this(name, SourceLevels.All)
		{
		}

		public Logger(string name, SourceLevels defaultLevel)
		{
			TraceSource = new TraceSource(name, defaultLevel);

			var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
			if (tier == Tier.Local)
			{
				ConfigureEmulatorTracing();
			}

			var appInsightsInstrumentationKey = EncryptedConfigurationManager.GetSetting(BaseAppSetting.AppInsightsInstrumentationKey);
			if (!string.IsNullOrWhiteSpace(appInsightsInstrumentationKey)
				&& !TelemetryConfiguration.Active.DisableTelemetry)
			{
				ConfigureAppInsightsTracing(appInsightsInstrumentationKey);
			}
		}

		private void ConfigureEmulatorTracing()
		{
			var type = Type.GetType("Microsoft.ServiceHosting.Tools.DevelopmentFabric.Runtime.DevelopmentFabricTraceListener, Microsoft.ServiceHosting.Tools.DevelopmentFabric.Runtime, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");
			if (type == null)
				return;
			var listener = Activator.CreateInstance(type) as TraceListener;
			if (listener == null)
				return;
			TraceSource.Listeners.Add(listener);
			Trace.Listeners.Add(listener);
		}

		private void ConfigureAppInsightsTracing(string appInsightsInstrumentationKey)
		{
			var aiListener = new ApplicationInsightsTraceListener(appInsightsInstrumentationKey);
			TraceSource.Listeners.Add(aiListener);
			Trace.Listeners.Add(aiListener);
		}

		private void TraceEvent(TraceEventType eventType, string message)
		{
			TraceSource.TraceEvent(eventType, 0, message);
		}

		private void TraceEvent(TraceEventType eventType, string format, params object[] args)
		{
			TraceSource.TraceEvent(eventType, 0, format, args);
		}

		private void TraceData(TraceEventType eventType, object data)
		{
			TraceSource.TraceData(eventType, 0, data);
		}

		private void TraceData(TraceEventType eventType, params object[] data)
		{
			TraceSource.TraceData(eventType, 0, data);
		}

		public void Debug(string message)
		{
			TraceEvent(TraceEventType.Verbose, message);
		}

		public void Debug(string format, params object[] args)
		{
			TraceEvent(TraceEventType.Verbose, format, args);
		}

		public void Debug(object data)
		{
			TraceData(TraceEventType.Verbose, data);
		}

		public void Debug(params object[] data)
		{
			TraceData(TraceEventType.Verbose, data);
		}

		public void Info(string message)
		{
			TraceEvent(TraceEventType.Information, message);
		}

		public void Info(string format, params object[] args)
		{
			TraceEvent(TraceEventType.Information, format, args);
		}

		public void Info(object data)
		{
			TraceData(TraceEventType.Information, data);
		}

		public void Info(params object[] data)
		{
			TraceData(TraceEventType.Information, data);
		}

		public void Warn(string message)
		{
			TraceEvent(TraceEventType.Warning, message);
		}

		public void Warn(string format, params object[] args)
		{
			TraceEvent(TraceEventType.Warning, format, args);
		}

		public void Warn(object data)
		{
			TraceData(TraceEventType.Warning, data);
		}

		public void Warn(params object[] data)
		{
			TraceData(TraceEventType.Warning, data);
		}

		public void Error(string message)
		{
			TraceEvent(TraceEventType.Error, message);
		}

		public void Error(string format, params object[] args)
		{
			TraceEvent(TraceEventType.Error, format, args);
		}

		public void Error(object data)
		{
			TraceData(TraceEventType.Error, data);
		}

		public void Error(params object[] data)
		{
			TraceData(TraceEventType.Error, data);
		}

		public void Critical(string message)
		{
			TraceEvent(TraceEventType.Critical, message);
		}

		public void Critical(string format, params object[] args)
		{
			TraceEvent(TraceEventType.Critical, format, args);
		}

		public void Critical(object data)
		{
			TraceData(TraceEventType.Critical, data);
		}

		public void Critical(params object[] data)
		{
			TraceData(TraceEventType.Critical, data);
		}
	}
}